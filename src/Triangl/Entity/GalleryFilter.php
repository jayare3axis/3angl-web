<?php

namespace Triangl\Entity;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Filtres gallery items by gallery.
 */
class GalleryFilter extends SQLFilter {
    /**
     * Implemented.
     */
    public function addFilterConstraint(ClassMetaData $targetEntity, $targetTableAlias)
    {
        if ( $targetEntity->getName() != 'Triangl\Entity\Website\Image' ) {
            return '';
        }
        return $targetTableAlias.'.gallery_id = ' . $this->getParameter('gallery_id');
    }
}
