<?php

namespace Triangl\Entity;

use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * Handles events for website entities.
 */
class WebsiteEntityEventSubscriber implements EventSubscriber {
    /**
     * Implemented.
     */
    public function postLoad(LifecycleEventArgs $event) {
        
    }
 
    /**
     * Implemented.
     */
    public function getSubscribedEvents() {
        return [Events::postLoad];
    }
}
