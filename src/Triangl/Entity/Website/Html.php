<?php

namespace Triangl\Entity\Website;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Html component entity.
 * @Entity @Table(name="htmls")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Html extends Component {
    use ORMBehaviors\Translatable\Translatable;
}
