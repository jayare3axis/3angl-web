<?php

namespace Triangl\Entity\Website;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\OrderTrait;
use Triangl\Entity\Website\Article;
use Triangl\Entity\Website\Component;

/**
 * Association between article and its components.
 * @Entity @Table(name="article_component")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 */
class ArticleComponentAssociation {
    use PrimaryIdTrait;
    use OrderTrait;
    
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Website\Article", inversedBy="article_component_associations")
     * @JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;
    
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Website\Component", inversedBy="article_component_associations")
     * @JoinColumn(name="component_id", referencedColumnName="id")
     */
    private $component;
    
    /**
     * Set article.
     * @param \Triangl\Entity\Website\Article $article
     * @return \Triangl\Entity\Website\ArticleComponentAssociation this
     */
    public function setArticle(Article $article = null)
    {
        $this->article = $article; 
        return $this;
    }
 
    /**
     * Get article
     * @return \Triangl\Entity\Website\Article
     */
    public function getArticle()
    {
        return $this->article;
    }
    
    /**
     * Set component.
     * @param \Triangl\Entity\Website\Component $component
     * @return \Triangl\Entity\Website\ArticleComponentAssociation this
     */
    public function setComponent(Component $component = null) {
        $this->component = $component; 
        return $this;
    }
 
    /**
     * Get section
     * @return \Triangl\Entity\Website\Component
     */
    public function getComponent() {
        return $this->component;
    }
}
