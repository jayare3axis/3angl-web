<?php

namespace Triangl\Entity\Website;

use Doctrine\Common\Collections\ArrayCollection;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\BelongsToDomainTrait;
use Triangl\Entity\BelongsToDomainInterface;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Article entity.
 * @Entity @Table(name="articles")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Article implements BelongsToDomainInterface {
    use PrimaryIdTrait;  
    use BelongsToDomainTrait;
    use ORMBehaviors\Translatable\Translatable;
    
    /** @Column(type="string") **/
    protected $layout;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Website\SectionArticleAssociation", mappedBy="article")
     */
    protected $section_article_associations;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Website\ArticleComponentAssociation", mappedBy="article")
     * @OrderBy({"ord" = "ASC"})
     */
    protected $article_component_associations;
        
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->layout = "";
        $this->section_article_associations = new ArrayCollection();
        $this->article_component_associations = new ArrayCollection();        
    }
    
    /**
     * Gets the layout.
     * @return string
     */
    public function getLayout() {
        return $this->layout;
    }

    /**
     * Sets the layout.
     * @param string $name
     */
    public function setLayout($layout) {
        $this->layout = $layout;
    }
        
    /**
     * Gets section-article associations.
     * @return Triangl\Entity\Website\SectionArticleAssociation
     */
    public function getSectionArticleAssociations() {
        return $this->section_article_associations;
    }
    
    /**
     * Gets article-component associations.
     * @return Triangl\Entity\Website\ArticleComponentAssociation
     */
    public function getArticleComponentAssociations() {
        return $this->article_component_associations;
    }
}
