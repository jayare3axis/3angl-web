<?php

namespace Triangl\Entity\Website;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Html translation entity.
 * @Entity @Table(name="html_translations")
 **/
class HtmlTranslation {    
    use ORMBehaviors\Translatable\Translation;
    
    /** @Column(type="text") **/
    protected $content;
    
    /**
     * Gets the content.
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Sets the content.
     * @param string $content
     */
    public function setContent($content) {
        $this->content = $content;
    }
}
