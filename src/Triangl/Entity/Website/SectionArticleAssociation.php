<?php

namespace Triangl\Entity\Website;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\OrderTrait;
use Triangl\Entity\Website\Section;
use Triangl\Entity\Website\Article;

/**
 * Association between set-top box and playlist.
 * @Entity @Table(name="section_article")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 */
class SectionArticleAssociation {
    use PrimaryIdTrait;
    use OrderTrait;
    
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Website\Section", inversedBy="section_article_associations")
     * @JoinColumn(name="section_id", referencedColumnName="id")
     */
    private $section;
 
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Website\Article", inversedBy="section_article_associations")
     * @JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;
    
    /**
     * Set section.
     * @param \Triangl\Entity\Website\Section $section
     * @return \Triangl\Entity\Website\SectionArticleAssociation this
     */
    public function setSection(Section $section = null) {
        $this->section = $section; 
        return $this;
    }
 
    /**
     * Get section
     * @return \Triangl\Entity\Website\Section
     */
    public function getSection() {
        return $this->section;
    }
 
    /**
     * Set article.
     * @param \Triangl\Entity\Website\Article $article
     * @return \Triangl\Entity\Website\SectionArticleAssociation this
     */
    public function setArticle(Article $article = null)
    {
        $this->article = $article; 
        return $this;
    }
 
    /**
     * Get article
     * @return \Triangl\Entity\Website\Article
     */
    public function getArticle()
    {
        return $this->article;
    }
}
