<?php

namespace Triangl\Entity\Website;

/**
 * Gallery component entity.
 * @Entity @Table(name="galleries")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Gallery extends Component {
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Website\Image", mappedBy="gallery")
     **/
    private $images;

    /**
     * Default constructor.
     */
    public function __construct() {
        $this->images = new ArrayCollection();
    }
    
    /**
     * Gets gallery images.
     */
    public function getImages() {
        return $this->images;
    }
}
