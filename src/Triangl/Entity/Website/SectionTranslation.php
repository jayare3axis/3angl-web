<?php

namespace Triangl\Entity\Website;

use Triangl\Entity\NameTrait;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Section translation entity.
 * @Entity @Table(name="section_translations")
 **/
class SectionTranslation {    
    use ORMBehaviors\Translatable\Translation;
    use NameTrait;    
}
