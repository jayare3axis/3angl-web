<?php

namespace Triangl\Entity\Website;

use Doctrine\Common\Collections\ArrayCollection;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\BelongsToDomainTrait;
use Triangl\Entity\BelongsToDomainInterface;

use Knp\DoctrineBehaviors\Model\Tree\NodeInterface;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Section entity.
 * @Entity @Table(name="sections")
 * @Entity(repositoryClass="Triangl\Entity\Website\SectionRepository")
 **/
class Section implements BelongsToDomainInterface, NodeInterface, \ArrayAccess {    
    use ORMBehaviors\Tree\Node;
    use ORMBehaviors\Translatable\Translatable;
    use PrimaryIdTrait;   
    use BelongsToDomainTrait;
    
    /** @Column(type="string") **/
    protected $materializedPath;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Website\SectionArticleAssociation", mappedBy="section")
     * @OrderBy({"ord" = "ASC"})
     */
    protected $section_article_associations;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->section_article_associations = new ArrayCollection();
        $this->materializedPath = "";
    }
    
    /**
     * Gets section-article associations.
     * @return Triangl\Entity\Website\SectionArticleAssociation
     */
    public function getSectionArticleAssociations() {
        return $this->section_article_associations;
    }
}
