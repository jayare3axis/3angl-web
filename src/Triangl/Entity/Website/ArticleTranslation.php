<?php

namespace Triangl\Entity\Website;

use Triangl\Entity\NameTrait;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Article translation entity.
 * @Entity @Table(name="article_translations")
 **/
class ArticleTranslation {    
    use ORMBehaviors\Translatable\Translation;
    use NameTrait;    
}
