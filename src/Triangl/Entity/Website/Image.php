<?php

namespace Triangl\Entity\Website;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\OrderTrait;
use Triangl\Entity\FileTrait;
use Triangl\Entity\Website\Gallery;

/**
 * Gallery image entity.
 * @Entity @Table(name="images")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Image {
    use PrimaryIdTrait;    
    use OrderTrait;
    use FileTrait;
    
    /**
     * @ManyToOne(targetEntity="\Triangl\Entity\Website\Gallery", inversedBy="images")
     * @JoinColumn(name="gallery_id", referencedColumnName="id")
     **/
    private $gallery;
    
    /**
     * Sets gallery.
     * @param \Triangl\Entity\Website\Gallery $gallery
     * @return \Triangl\Entity\Website\Gallery this
     */
    public function setGallery(Gallery $gallery = null) {
        $this->gallery = $gallery;
        return $this;
    }
    
    /**
     * Gets gallery.
     * @return \Triangl\Entity\Website\Gallery
     */
    public function getGallery() {
        return $this->gallery;
    }
}
