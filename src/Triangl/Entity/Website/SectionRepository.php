<?php

namespace Triangl\Entity\Website;

use Triangl\Entity\EntityRepository;

use Knp\DoctrineBehaviors\ORM as ORMBehaviors;

/**
 * Base entity repository.
 */
class SectionRepository extends EntityRepository {
    use ORMBehaviors\Tree\Tree;
}
