<?php

namespace Triangl\Entity\Website;

use Doctrine\Common\Collections\ArrayCollection;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\BelongsToDomainTrait;
use Triangl\Entity\BelongsToDomainInterface;
use Triangl\Entity\AliasTrait;

/**
 * Component entity.
 * @Entity @Table(name="components")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"html" = "Html", "gallery" = "Gallery", "attachment" = "Attachment"})
 **/
class Component implements BelongsToDomainInterface {
    use PrimaryIdTrait;
    use BelongsToDomainTrait;
    use AliasTrait;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Website\ArticleComponentAssociation", mappedBy="component")
     */
    protected $article_component_associations;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->article_component_associations = new ArrayCollection();
    }
    
    /**
     * Gets article-component associations.
     * @return Triangl\Entity\Website\ArticleComponentAssociation
     */
    public function getArticleComponentAssociations() {
        return $this->article_component_associations;
    }
}
