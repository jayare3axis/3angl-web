<?php

namespace Triangl;

use Silex\ServiceProviderInterface;

use Symfony\Component\HttpFoundation\Request;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemLeaf;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\BuildGridEvent;
use Triangl\Component\BuildBackendMenuEvent;
use Triangl\Entity\WebsiteEntityEventSubscriber;

/*
 * Triangl website module.
 */
class Website implements ServiceProviderInterface {
    private $alias;
    
    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {
        $app['web.domain'] = $app['db.orm.em']->getRepository('Triangl\Entity\Security\Domain')->findOneBy( array("alias" => $this->alias) );
        
        // Set title by domain.
        $app["name"] = $app['web.domain']->getName();
        
        // TO - DO only for La Maler
        $app["lamaler.menu"] = $app->share(function() use ($app) {
            $contents = file_get_contents('https://api.lunchtime.cz/export/restaurant.xml?id=6258');
            $xml = simplexml_load_string($contents);
            return "";
        });
    }

    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {
        // Website configurations
        $app['bootstrap.theme'] = 'spacelab';
        if ( $app->isDebug() ) {
            $app['asset.path'] = 'http://localhost/assets';
            $app['bootstrap.path'] = 'http://localhost/assets/bootstrap-3.2.0-dist';   
            $app['jq.file.upload.path'] = 'http://localhost/assets/jQuery-File-Upload-9.8.0';
            $app['jssor.path'] = 'http://localhost/assets/Jssor.Slider.FullPack';
            $app['backend.upload.path'] = 'http://localhost/3angl-web/web/uploads';
            $app['db'] = array(
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'dbname' => 'website',
                'user' => 'root',
                'password' => null,
            );
            $app['backend.upload.dir'] = $app['path'] . '/../web/uploads';
        }
        else {
            $app['asset.path'] = 'http://www.lipnoapartman.eu/assets';
            $app['bootstrap.path'] = 'http://www.lipnoapartman.eu/assets/bootstrap-3.2.0-dist';
            $app['jq.file.upload.path'] = 'http://www.lipnoapartman.eu/assets/jQuery-File-Upload-9.8.0';
            $app['jssor.path'] = 'http://www.lipnoapartman.eu/assets/Jssor.Slider.FullPack';
            $app['backend.upload.path'] = 'http://www.lipnoapartman.eu/uploads';
            $app['db'] = array(
                'driver' => 'pdo_mysql',
                'host' => 'wm79.wedos.net',
                'dbname' => 'd88376_lipno',
                'user' => 'w88376_lipno',
                'password' => 'UwGpSpXh',
            );
            $app['backend.upload.dir'] = $app['path'] . '/../www/uploads';
        }
                
        // Grid configuration.
        $app['dispatcher']->addListener('backend.build.grid', function (BuildGridEvent $event) {
            if ( $event->getClassName() == '\Triangl\Entity\Website\Image' ) {
                $event->setProperties( array('file') );
            }
        });
        
        // Backend navigation.
        $app['dispatcher']->addListener('backend.build.navigation', function (BuildBackendMenuEvent $event) {
            $this->buildBackendMenu( $event->getBuilder(), $event->getContext() );
        });                
        
        // Register entities.
        $app['triangl.entities']->addNamespace( array(
            'type' => 'annotation',
            'path' => __DIR__ . '/Entity/Website',
            'namespace' => 'Triangl\Entity\Website'
        ) );
        
        // Register filters.
        $app['triangl.entities']->addFilter('gallery', '\Triangl\Entity\GalleryFilter');
        
        // Choose domain
        // TO - DO fix this so in backend will be relevant title as chosen website
        //$this->alias = "LaMaler";
        $this->alias = "LipnoApartment";
        
        // Register template folder.
        $app['twig.loader.filesystem']->prependPath( __DIR__ . "/../../views/" . $this->alias );
        $app['twig.loader.filesystem']->prependPath(__DIR__ . "/../../views");
        
        // Services.
        $app['triangl.website'] = $app->share( function ($app) {
            return new WebsiteHelper($app);
        } );
        $websiteProvider = "\\Triangl\\" . $this->alias . "\\Provider\\" . $this->alias . "ServiceProvider";
        $app->register( new $websiteProvider() );   
                        
        // Controllers.
        $app['web.controller'] = $app->share(function() use ($app) {
            return new WebsiteController($app);
        });
        $app['backend.web.navigation.controller'] = $app->share(function() use ($app) {
            return new BackendNavigationWebsiteController($app);
        });
        
        // Routes
        $app->get("/", "web.controller:articleAction")
            ->value('id', -1)
            ->bind('homepage');
        $app->get("/article/{id}", "web.controller:articleAction")
            ->assert('id', '\d+')
            ->value('id', -1)
            ->bind('article');
        $app->get($app["backend.url"] . "/sections", "backend.web.navigation.controller:sectionsAction")
            ->bind('backend_sections');
        $app->get($app["backend.url"] . "/articles", "backend.web.navigation.controller:articlesAction")
            ->bind('backend_articles');
        $app->get($app["backend.url"] . "/galleries", "backend.web.navigation.controller:galleriesAction")
            ->bind('backend_galleries');
        
        // Events.
        $app["db.orm.em"]->getEventManager()->addEventSubscriber( new WebsiteEntityEventSubscriber() );
        
        // Middleware.
        $app->before(function (Request $request, \Silex\Application $app) {
            // Enable domain filter and set it's parameter to currentl website.
            $selectedDomain = $app['web.domain'];
            if ($selectedDomain) {
                $filter = $app['db.orm.em']->getFilters()->enable('domain');
                $filter->setParameter( 'domain_id', $selectedDomain->getId() );
            }
        });
    }
    
    /**
     * Builds backend menu.
     * @param \Triangl\Component\Navigation\MenuBuilder $builder
     * @param \Silex\Application $app
     */
    private function buildBackendMenu(MenuBuilder $builder, \Silex\Application $app) {
        $user = $app["security.user.instance"];
        $action = $app['triangl.security']->getActionByAlias('view');
                
        $menu = new MenuItemComposite("Content", null, "list-alt");
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Website\Section', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Sections", 'backend_sections'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Website\Article', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Articles", 'backend_articles'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Website\Gallery', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Galleries", 'backend_galleries'
            ) );            
        }
                
        $builder->pushChild($menu, true);
    }
}
