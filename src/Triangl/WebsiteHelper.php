<?php

namespace Triangl;

use Triangl\Entity\Website\Article;

/**
 * Helper routines for website module.
 */
class WebsiteHelper {
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Loads Jssor library.
     */
    public function loadJssor() {
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['jssor.path'] . '/js/jssor.js'
        ) );
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['jssor.path'] . '/js/jssor.slider.mini.js'
        ) );
    }
    
    /**
     * Gets article component by it's alias or null if not found.
     * @param \Triangl\Entity\Website\Article $article
     * @param string $alias
     * @return \Triangl\Entity\Website\Article
     */
    public function getComponentByAlias(Article $article, $alias) {
        foreach ( $article->getArticleComponentAssociations() as $association ) {
            $component = $association->getComponent();
            if ($component->getAlias() == $alias) {
                return $component;
            }
        }
        return null;
    }
}
