<?php

namespace Triangl\LipnoApartment\Entity;

use Triangl\Entity\EntityRepository;

/**
 * Lipno appartment booking day entity repository
 **/
class BookingDayRepository extends EntityRepository {
    /**
     * Finds booking days by month and year.
     * @param int $month
     * @param int $year
     * @return array
     */
    function findByMonth($month, $year) {
        $begin = new \DateTime();
        $begin->setDate($year, $month, 1);
        $end = new \DateTime();
        $end->setDate($year, $month, 31);
        
        // Create query.
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->select( array('o') )
                    ->from($this->getEntityName(), 'o')
                    ->where('o.date BETWEEN :begin AND :end')
                    ->setParameter( 'begin', $begin->format('Y-m-d') )
                    ->setParameter( 'end', $end->format('Y-m-d') );
        
        return $query->getQuery()->getResult();
    }
}
