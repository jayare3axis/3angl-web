<?php

namespace Triangl\LipnoApartment\Entity;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\DateTrait;

/**
 * Lipno appartment booking day entity
 * @Entity @Table(name="lipnoapartment_booking_day")
 * @Entity(repositoryClass="Triangl\LipnoApartment\Entity\BookingDayRepository")
 **/
class BookingDay {
    use PrimaryIdTrait;
    use DateTrait;
    
    /**
     * @ManyToOne(targetEntity="\Triangl\LipnoApartment\Entity\BookingDayStatus")
     * @JoinColumn(name="status_id", referencedColumnName="id")
     **/
    private $status;
    
    /**
     * Sets status.
     * @param \Triangl\LipnoApartment\Entity\BookingDayStatus
     * @return \Triangl\Entity\Security\Firewall this
     */
    public function setStatus(BookingDayStatus $status = null) {
        $this->status = $status;
        return $this;
    }
    
    /**
     * Gets status.
     * @return \Triangl\LipnoApartment\Entity\BookingDayStatus
     */
    public function getStatus() {
        return $this->status;
    }
}
