<?php

namespace Triangl\LipnoApartment\Entity;

use Triangl\Entity\NameTrait;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Lipno apartment booking day status entity.
 * @Entity @Table(name="lipnoapartment_bds_translations")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class BookingDayStatusTranslation {
    use ORMBehaviors\Translatable\Translation;
    use NameTrait;
}
