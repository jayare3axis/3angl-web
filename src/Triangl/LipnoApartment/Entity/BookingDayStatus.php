<?php

namespace Triangl\LipnoApartment\Entity;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\AliasTrait;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Lipno apartment booking day status entity.
 * @Entity @Table(name="lipnoapartment_booking_day_status")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class BookingDayStatus {    
    use PrimaryIdTrait;  
    use AliasTrait;
    use ORMBehaviors\Translatable\Translatable;
    
    /**
     * Implemented.
     */
    public function __toString() {
        // TO - DO get current locale
        return $this->translate("cs")->getName();
    }
}
