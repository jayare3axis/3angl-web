<?php

namespace Triangl\LipnoApartment;

use Triangl\Controller;

/*
 * Triangl backend controller.
 */
class BackendNavigationController extends Controller {
    /**
     * Booking days action.
     */
    public function bookingDaysAction() {
        $className = '\Triangl\LipnoApartment\Entity\BookingDay';
        
        $this->app['navigation.backend']->selectByRoute('backend_booking_days');
        return $this->app['twig']->render( 'backend_content_single_grid_edit.html.twig', array(
            'className' => $className,
            'grid_menu' => $this->app['db.orm.grid.menu']->createMenu($className)
        ) );
    }
}
