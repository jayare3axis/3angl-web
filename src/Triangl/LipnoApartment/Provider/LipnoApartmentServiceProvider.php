<?php

namespace Triangl\LipnoApartment\Provider;

use Silex\ServiceProviderInterface;

use Triangl\LipnoApartment\BackendNavigationController;
use Triangl\LipnoApartment\BookingDaysController;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemLeaf;
use Triangl\Component\BuildBackendMenuEvent;

/**
 * Backend api provider.
 */
class LipnoApartmentServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app["lipnoapartment.booking_days"] = $app->share(function() use ($app) {            
            return $app['lipnoapartment.booking_days.controller']->indexAction();
        });
        
        // Register entities.
        $app['triangl.entities']->addNamespace( array(
            'type' => 'annotation',
            'path' => __DIR__ . '/../Entity',
            'namespace' => 'Triangl\LipnoApartment\Entity'
        ) );
        
        // Backend navigation.
        $app['dispatcher']->addListener('backend.build.navigation', function (BuildBackendMenuEvent $event) {
            $this->buildBackendMenu( $event->getBuilder(), $event->getContext() );
        });
        
        // Controllers.
        $app['lipnoapartment.booking_days.controller'] = $app->share(function() use ($app) {
            return new BookingDaysController($app);
        });
        $app['backend.lipnoapartment.controller'] = $app->share(function() use ($app) {
            return new BackendNavigationController($app);
        });
        
        // Routes
        $app->get($app["backend.url"] . "/bookingDays", "backend.lipnoapartment.controller:bookingDaysAction")
            ->bind('backend_booking_days');
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
    
    /**
     * Builds backend menu.
     * @param \Triangl\Component\Navigation\MenuBuilder $builder
     * @param \Silex\Application $app
     */
    private function buildBackendMenu(MenuBuilder $builder, \Silex\Application $app) {
        $user = $app["security.user.instance"];
        $action = $app['triangl.security']->getActionByAlias('view');
        
        $menu = $builder->getChild(1);
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\LipnoApartment\Entity\BookingDay', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Booking days", 'backend_booking_days'
            ) );
        }
    }
}
