<?php

namespace Triangl\LipnoApartment;

use Triangl\Controller;

/*
 * Triangl booking days controller.
 */
class BookingDaysController extends Controller {
    /**
     * Booking days action.
     */
    public function indexAction() {
        $year = 2015;
        $month = 1;
        
        $bookingDays = $this->app["db.orm.em"]->getRepository("Triangl\LipnoApartment\Entity\BookingDay")->findByMonth($month, $year);
        
        return $this->app['twig']->render( 'booking_days.html.twig', array(
            'bookingDays' => $bookingDays,
            'month' => $month,
            'monthName' => $this->getNameOfMonth($month),
            'weekDay' => JDDayOfWeek( gregoriantojd($month, 1, $year) ),
            'weekDays' => $this->getWeekDays(),
            'monthDays' => cal_days_in_month(CAL_GREGORIAN, $month, $year),
            'year' => $year
        ) );
    }
    
    private function getNameOfMonth($index)
    {
        // TO - DO to be obsolete
        switch($index) {
            case 1:
                return "Leden";
            case 2:
                return "Únor";
            case 3:
                return "Březen";
            case 4:
                return "Duben";
            case 5:
                return "Květen";
            case 6:
                return "Červen";
            case 7:
                return "Červenec";
            case 8:
                return "Srpen";
            case 9:
                return "Září";
            case 10:
                return "Říjen";
            case 11:
                return "Listopad";
            case 12:
                return "Prosinec";
        }
    }
    
    private function getWeekDays()
    {
        return array("Po", "Út", "St", "Čt", "Pá", "So", "Ne");
    }
}
