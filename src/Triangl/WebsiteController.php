<?php

namespace Triangl;

use Triangl\Entity\Website\Section;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\Navigation\MenuItemLeaf;

use Knp\DoctrineBehaviors\Model\Tree\NodeInterface;

/*
 * Controller for front-end navigation.
 */

class WebsiteController extends Controller {

    /**
     *  Article action.
     */
    public function articleAction($id) {
        // TO - DO handle current locale
        $loc = "cs";     
        
        $em = $this->app["db.orm.em"];
        
        /*$html = new \Triangl\Entity\Website\Html();
        $html->setAlias('content');
        $content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sem libero, lobortis eget mi vitae, lobortis convallis purus. Nullam in dolor ac erat rhoncus aliquam. Nam tincidunt ante venenatis, dictum odio vitae, cursus libero. Morbi at nunc sit amet nisi euismod consectetur consequat a lectus. Curabitur sed accumsan mauris. Pellentesque eu lacinia quam, in maximus nisi. Etiam pharetra lacus non nisi semper pulvinar. Ut a ullamcorper nunc, nec maximus sem. Donec id purus nec ipsum imperdiet pellentesque. Vivamus sed sem bibendum elit condimentum consectetur.</p>";
        $html->translate($loc)->setContent($content);
        $html->mergeNewTranslations();
        $em->persist($html);
        $em->flush();*/
        
        /*$html = $em->getRepository("Triangl\Entity\Website\Html")->find(24);
        $content = '<h3>Volné termíny</h3><p>&nbsp;</p><p>{{ app["lipnoapartment.booking_days"]|raw }}</p>';
        $html->translate($loc)->setContent($content);
        $html->mergeNewTranslations();
        $em->persist($html);
        $em->flush();*/
        
        /*$article = new \Triangl\Entity\Website\Article();
        $article->translate('cs')->setName('Úvod');
        $article->mergeNewTranslations();
        $em->persist($article);
        $em->flush();*/
                        
        /*$root = new \Triangl\Entity\Website\Section();
        $root->translate('cs')->setName('Lipno Apartmán');
        
        $restaurant = new \Triangl\Entity\Website\Section();
        $restaurant->translate('cs')->setName('Restaurace');
        
        $club = new \Triangl\Entity\Website\Section();
        $club->translate('cs')->setName('Klub');
        
        $em->persist($root);
        $em->persist($restaurant);
        $em->persist($club);
        
        $root->mergeNewTranslations();
        $restaurant->mergeNewTranslations();
        $club->mergeNewTranslations();
        $em->flush();
        
        $restaurant->setChildNodeOf($root);
        $club->setChildNodeOf($root);
        
        $em->persist($restaurant);
        $em->persist($club);
        $em->flush();*/
        
        // TO - DO determine theme by domain
        $this->app['bootstrap.theme'] = 'state';

        // Init assets.        
        $this->app['triangl.bootstrap']->loadBootstrap();
        $this->app['triangl.website']->loadJssor();
        $this->app["assets"]->addStyleSheet(array(
            "template" => "web.css.twig"
        ));

        // TO - DO determine default article id
        if ($id == -1) {
            $id = 9;
        }

        $builder = $this->buildNavigation($id);
        
        $article = $em->getRepository('\Triangl\Entity\Website\Article')->find($id);
        $layout = $article->getLayout();
        $params = array(
            "navigation" => $builder,
            "loc" => $loc,
            "article" => $article,
            "gallery" => $this->loadArticleGallery($id)
        );
        
        return $this->app['twig']->render($layout, $params);
    }

    /**
     * @ignore
     */
    private function buildNavigation($id) {
        // TO - DO handle current locale
        $loc = "cs";     
        
        $em = $this->app["db.orm.em"];
        
        $root = $em->getRepository("\Triangl\Entity\Website\Section")->getTree();
        
        $builder = new MenuBuilder( $root->translate($loc)->getName() );
        $this->loadSection($root, $builder);
                
        $builder->selectByRoute("article", array("id" => $id));
                
        return $builder;
    }
    
    /**
     * @ignore
     */
    private function loadSection(NodeInterface $node, MenuItemComposite $parent) {
        // TO - DO handle current locale
        $loc = "cs";        
        
        if ( $node->isLeafNode() ) {
            $menu = new MenuItemComposite( $node->translate($loc)->getName() ); 
            $this->loadSectionArticles($node, $menu); 
            $parent->pushChild($menu);
        }
        else {            
            foreach ($node->getChildNodes() as $child) {
                $this->loadSection($child, $parent);  
            }            
        }
    }
    
    /**
     * @ignore
     */
    private function loadSectionArticles(Section $section, MenuItemComposite $menu) {
        // TO - DO handle current locale
        $loc = "cs";
        
        foreach( $section->getSectionArticleAssociations() as $association ) {
            $article = $association->getArticle();
            $item = new MenuItemLeaf(
                $article->translate($loc)->getName(), 'article'
            );
            $item->pushArg( "id", $article->getId() );
            $menu->pushChild($item);
        }
    }

    /**
     * @ignore
     */
    private function loadArticleGallery($id) {
        $path = $this->app["asset.path"] . "/LaMaler/gallery/";

        if ($id < 7) {
            return array($path . "LaMaler1.jpg", $path . "LaMaler2.jpg", $path . "LaMaler3.jpg", $path . "LaMaler4.jpg", $path . "LaMaler5.jpg",
                $path . "LaMaler6.jpg", $path . "LaMaler7.jpg", $path . "LaMaler8.jpg", $path . "LaMaler9.jpg", $path . "LaMaler10.jpg",
                $path . "LaMaler11.jpg", $path . "LaMaler12.jpg", $path . "LaMaler13.jpg", $path . "LaMaler14.jpg", $path . "LaMaler15.jpg",
                $path . "LaMaler16.jpg", $path . "LaMaler17.jpg", $path . "LaMaler18.jpg", $path . "LaMaler19.jpg", $path . "LaMaler20.jpg",
                $path . "LaMaler21.jpg");
        } else {
            return array($path . "Club1.jpg", $path . "Club2.jpg", $path . "Club3.jpg", $path . "Club4.jpg", $path . "Club5.jpg");
        }
    }

}
