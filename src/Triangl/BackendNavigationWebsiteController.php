<?php

namespace Triangl;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\Navigation\MenuItemLeaf;

/*
 * Controller for navigating between System menu.
 */
class BackendNavigationWebsiteController extends Controller {
    /**
     *  Sections action.
     */
    public function sectionsAction() {
        $className = '\Triangl\Entity\Website\Section';
        
        $this->app['navigation.backend']->selectByRoute('backend_sections');
        return $this->app['twig']->render( 'backend_content_single_grid_edit.html.twig', array(
            'className' => $className,
            'grid_menu' => $this->app['db.orm.grid.menu']->createMenu($className)
        ) );
    }    
    
    /**
     *  Articles action.
     */
    public function articlesAction() {
        $className = '\Triangl\Entity\Website\Article';
        
        $this->app['navigation.backend']->selectByRoute('backend_articles');
        return $this->app['twig']->render( 'backend_content_single_grid_edit.html.twig', array(
            'className' => $className,
            'grid_menu' => $this->app['db.orm.grid.menu']->createMenu($className)
        ) );
    }
    
    /**
     *  Galleries action.
     */
    public function galleriesAction() {        
        $className1 = '\Triangl\Entity\Website\Gallery';
        $className2 = '\Triangl\Entity\Website\Image';
        
        $galleryId = null;
        $repository = $this->app['db.orm.em']->getRepository($className1);
        $galleries = $repository->findAll();
        if ( count($galleries) > 0 ) {
            $galleryId = $galleries[0]->getId();
        }
        
        $itemsMenu = $this->app['db.orm.grid.menu']->createMenu($className2);
        $menu = new MenuItemComposite("Order", null);
        $item = new MenuItemLeaf(
            '', 'change_order', 'arrow-up'
        );
        $item->pushArg('className', $className2);
        $item->pushArg('method', 'up');
        $item->addData('btn-type', 'info');
        $item->addClass('tool-move-up');
        $menu->pushChild($item);
        $item = new MenuItemLeaf(
            '', 'change_order', 'arrow-down'
        );
        $item->pushArg('className', $className2);
        $item->pushArg('method', 'down');
        $item->addData('btn-type', 'info');        
        $item->addClass('tool-move-down');
        $menu->pushChild($item);
        $itemsMenu->pushChild($menu);
        
        $this->app['navigation.backend']->selectByRoute('backend_galleries');
        return $this->app['twig']->render( 'backend_content_categorized_grid_edit.html.twig', array(
            'className1' => $className1,
            'className2' => $className2,
            'grid_menu1' => $this->app['db.orm.grid.menu']->createMenu($className1),
            'grid_menu2' => $itemsMenu,
            'orderBy' => 'ord:ASC',
            'filter' => 'gallery',
            'filterParam' => 'gallery_id',
            'filterValue' => $galleryId,
            'filterProp' => 'gallery'
        ) );
    }
}
