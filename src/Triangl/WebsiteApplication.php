<?php

namespace Triangl;

/**
 * Website application.
 */
class WebsiteApplication extends BackendApplication {
    /**
     * Overriden.
     */
    protected function init() {
        parent::init();
        
        $this->register( new Website() );
    }
    
    /**
     * Overriden.
     */
    public function isDebug() {
        return parent::isDebug();
    }
}
