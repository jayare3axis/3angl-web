<?php

require_once __DIR__ . '/vendor/autoload.php';

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Triangl\WebsiteApplication;

$app = new WebsiteApplication( __DIR__ . '/var', array('test' => true) );
$app->boot();

return ConsoleRunner::createHelperSet($app['db.orm.em']);
