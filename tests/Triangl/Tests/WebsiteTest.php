<?php

namespace Triangl\Tests;

use Triangl\WebTestCase;
use Triangl\WebsiteApplication;

/**
 * Functional test for Triangl Website.
 */
class WebsiteTest extends WebTestCase {
    /**
     * Implemented.
     */
    public function createApplication() {
        return new WebsiteApplication( 
            __DIR__ . "/../../../var", 
            array("test" => true) 
        );
    }
}
